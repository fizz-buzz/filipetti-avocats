---
title: Coordonnées et contact
description: Nous intervenons dans les principaux tribunaux du 06, du 83, et du 13,
  ainsi qu'à Monaco.
bg_image: images/featue-bg.jpg
aliases:
- "/bureaux/"
rdv_en_ligne:
- title: Prendre RDV en ligne
  descr: Maître Filipetti vous reçoit à son cabinet pour un 1er rendez-vous de 60
    min, sans engagement, qui permet réciproquement de faire connaissance, de connaître
    vos droits et la manière de traiter la problématique à laquelle vous êtes confronté.
  link: https://consultation.avocat.fr/avocat-44892-7e6d.html
  id: 1
- title: Demander un rappel
  descr: Maître Filipetti vous rappelle pour répondre à vos questions juridiques.
    Cette consultation téléphonique de 20 min vous permet d'obtenir des éléments de
    réponse concrets et rapides à la question que vous vous posez.
  link: https://consultation.avocat.fr/avocat-44892-7e6d.html
  id: 2
- title: Consulter par écrit
  descr: Maître Filipetti répond à vos questions juridiques par écrit. Si vous souhaitez
    accompagner votre question d'une pièce jointe, la consultation juridique est adaptée,
    sinon la question simple (moins de 1.000 caractères) répondra parfaitement à votre
    question de droit.
  link: https://consultation.avocat.fr/avocat-44892-7e6d.html
  id: 3
meta_description: ''
menu:
  footer1:
    name: Contact
    weight: 3
  main:
    name: Contact
    URL: contact
    weight: 4

---
