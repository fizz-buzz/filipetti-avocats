---
description: Au service de nos clients depuis 2004.
meta_description: Isabelle FILIPETTI a prêté serment à Paris en 2004 et a été une
  collaboratrice engagée au sein de cabinets parisiens et niçois renommés. En 2010,
  elle crée son cabinet en s’entourant d’une équipe dynamique partageant son niveau
  d’exigence.
title: Cabinet & Equipe
layout: equipe
image: images/company/bureau3.jpg
aliases:
- "/equipe/"
collaborateurs:
- nom: Isabelle FILIPETTI
  ordre : 1
  bio: Isabelle FILIPETTI est avocat au Barreau de Grasse (06). Titulaire d’une maîtrise
    en droit privé et d’un DESS de droit de Propriété Intellectuelle et du multimédia
    de l’Université Panthéon-Assas (Paris II), Isabelle a prêté serment à Paris en
    2004 et a été une collaboratrice engagée au sein de cabinets parisiens et niçois
    renommés.
  photo: "/images/teams/avocat-isabelle-filipetti.jpg"
  titre: " Avocat Fondateur"
  devise: Quoi que tu rêves d'entreprendre, commence-le. L'audace a du génie, du pouvoir,
    de la magie. <b>Goethe</b>
- nom: Vanessa DIDIER
  ordre : 2
  bio: Vanessa DIDIER est avocat depuis 2004. Elle a travaillé pendant de nombreuses
    années dans un cabinet renommé à Montpellier en droit des affaires avant de nous
    rejoindre. Vanessa gère principalement les contentieux généraux et issus du droit
    des affaires/commercial. Vanessa est également spécialiste du droit de la copropriété
    et du droit de l'immobilier.
  photo: "/images/teams/vanessa.jpg"
  titre: 'Avocat Associé'
  devise: " Il faut faire le bien pour mériter son bonheur, on n'y arrive pas par
    la spéculation et la paresse. La paresse séduit et le travail satisfait. C'est
    dans une conscience tranquille qu'on puise sa force. <b>Journal de Anne Frank</b> "
- nom: Méganne LEYMONERIE 
  ordre : 3
  bio: 
  photo: "https://placehold.co/350x326?text=A%20venir"
  titre: 'Avocat collaborateur en droit des affaires'
  devise: 'A venir !'
- nom: Collaborateur 
  ordre : 3
  bio: 
  photo: "https://placehold.co/350x326?text=A%20venir"
  titre: 'Avocat collaborateur en droit du travail'
  devise: 'A venir !'
- nom: 'Juristes stagiaires '
  ordre : 5
  bio: Le Cabinet attache une importance particulière à la formation des juristes.
    Le savoir doit se partager réciproquement et s'apprécier dans sa pratique. Nous
    recrutons régulièrement des stagiaires en master II et/ou école d'avocat.
  photo: "/images/slider-bg.jpg"
  titre: Nos petites aides et des avocats en devenir...
  devise: Le secret du changement consiste à concentrer son énergie pour créer du
    nouveau, et non pas pour se battre contre l'ancien. <b>Dan Millman</b>
- nom: 'NH Avocats '
  ordre : 4
  bio: Le Cabinet travaille en synergie avec NH Avocats afin de vous apporter une
    réelle expertise en droits des affaires et en étroite collaboration avec d’autres
    cabinets d’avocats sis notamment à Paris, Lyon et Marseille.
  photo: "/images/teams/nh-avocats.jpg"
  titre: Avocat société
  devise: Seul on va plus vite, à deux on va plus loin… Principe de l’intelligence
    collective
collaborateurs_suite: Nous sommes également en étroite collaboration avec un réseau
  de confiance composé de cabinets d’avocats sis notamment à Paris, Lyon, et Marseille.
valeurs:
  description: Toute l’équipe se tient prête pour vous accompagner dans une relation
    de confiance durable et de disponibilité permanente…
  enable: true
  title: Nos valeurs, nos engagements
  image_section: "/images/company/bureau3.jpg"
  valeur_item:
  - title: 'Nos avocats sont :'
    adjectives:
    - name: Disponibles
    - name: Réactifs
    - name: Loyaux
  - title: 'Nous travaillons dans :'
    adjectives:
    - name: La transparence
    - name: La bonne humeur
    - name: L’écoute et l’empathie
  - title: 'Nous cultivons :'
    adjectives:
    - name: La rigueur
    - name: La polyvalence
    - name: Le pragmatisme et la créativité
honoraires:
  title: Honoraires
  content: ''
menu:
  main:
    name: Equipe
    URL: cabinet
    weight: 1
  footer1:
    name: Equipe
    weight: 1

---
<b>Les cabinets IF Avocats et NH avocats ont fusionné en 2022 pour devenir AXE AVOCATS</b>

Nous sommes un cabinet à taille humaine joignable, proche de vous, réactif, et à votre disposition sans gêne. Notre volonté est de collaborer efficacement avec vous en étant accessible et en restant à votre écoute pour maîtriser les risques tout en développant des stratégies utiles et efficaces à vos problématiques afin de vous accompagner avec audace et perspicacité.

Communiquer encore et toujours…

Maîtriser les risques…

Soyons audacieux et osons ensemble…

Construisons sereinement ce que vous désirez, souhaitez et ambitionnez…

Nous sommes là pour vous et avec vous.