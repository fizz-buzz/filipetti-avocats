---
title: Honoraires
description: Notre offre à destination des particuliers et des professionnels
meta_description: Notre politique tarifaire se doit d’être fondée sur la confiance.
  Notre but est que vous ne soyez pas gêné de nous appeler et de faire appel à nous
  en toutes circonstances.
intro_titre: Pour les professionels et les particuliers
intro_texte: "**Notre politique tarifaire se doit d’être fondée sur la confiance.**
  \n\nNotre but est que vous ne soyez pas gêné de nous appeler et de faire appel à
  nous en toutes circonstances. Faisons simple et efficace avec comme engagement la
  communication et la transparence. Ainsi, en fonction de la nature de votre dossier,
  nous vous proposons 4 options."
offres:
- pack: Maîtrise
  description: "« Pack » d’heures pour un besoin spécifique avec 10% de remise sur
    le taux horaire habituel HT."
- pack: Sérénité
  description: Abonnement à l’année avec 15% de remise sur le taux horaire habituel
    HT.
- pack: Confiance
  description: Forfait avec éventuellement honoraires de résultat sur devis (majoritairement
    pour les contentieux).
- pack: Au temps
  description: 'Facturation au temps passé à partir d’un taux horaire de 250 Euros
    HT. '
conditions: "**Des frais administratifs peuvent être facturés en sus au client.**
  \n\nIls couvrent les frais de secrétariat, de communication, de reproduction de
  documents et autres frais administratifs supportés par le cabinet dans le cadre
  de la réalisation des prestations.\n\nLes frais exposés auprès de tiers (frais de
  déplacement, d’huissiers, de traduction, etc ) sont refacturés au client sur la
  base des justificatifs."
menu:
  footer1:
    weight: 2
  main:
    name: Offres
    weight: 2

---
