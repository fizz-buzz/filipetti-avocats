+++
aliases = ["/avocat-droit-social"]
bgcolor = "#e76f51"
description = "Conseil aux salariés et employeurs, assistance devant le conseil de prud'hommes"
h3 = "Droit du travail"
icon = "/images/icons/icon_droit-travail.png"
meta_description = "Conseil aux salariés et employeurs, assistance devant le conseil de prud'hommes."
slug = "avocat-droit-du-travail"
title = "Avocat en droit du travail et droit social"
weight = 3
id_expertise = 3
[menu.footer2]
name = "Droit du travail"
weight = 3
[menu.main]
name = "Droit du travail"
parent = "Expertises"
weight = 3

+++
### Conseil et contentieux
Le cabinet a fait de cette matière une de ses activités dominantes, au service des entreprises et des particuliers.  Selon les affaires confiées au cabinet, nous défendons les intérêts de l’employeur et du salarié.

### Relation de travail
Le cabinet intervient dans toutes les étapes liées aux rapports individuels et collectifs entre salariés et employeurs notamment :

* Embauche
* Licenciement
* Procédure disciplinaire
* Démission
* Négociation - transaction
* Rupture conventionnelle
* Harcèlement
* Sécurité au travail
* Non paiement de primes
* Non paiement de salaires, heures supplémentaires
* Analyse des clauses particulières du contrat : clause de non-concurrence, loyauté, mobilité, etc.


### Contrats de travail
Le cabinet intervient dans l’élaboration, la négociation, et la rédaction des contrats de travail. Il peut également analyser des contrats et réaliser les audits en entreprises en matière sociale.

### Employeurs
Nous pouvons vous accompagner au quotidien pour vos questions juridiques, grâce à un système d’abonnement juridique.