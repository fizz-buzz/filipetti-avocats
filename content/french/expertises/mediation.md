+++
aliases = []
bgcolor = "#2a9d8f"
description = "Une expertise au service de la médiation"
h3 = "Mediation"
icon = "/images/icons/icon_mediation.png"
meta_description = ""
slug = "avocat-mediation"
title = "Mediation"
weight = 6
id_expertise = 5

[menu.footer2]
name = "Mediations"
weight = 6
[menu.main]
name = "Mediations"
parent = "Expertises"
weight = 6

+++
Isabelle Filipetti peut vous assister en tant que médiateur, co-médiateur ou dans le cadre d’une négociation collaborative pour vos problématiques personnelles ou collectives.


La médiation est une alternative à tous litiges dont vous serez le seul acteur, pensez-y…