---
title: Tous nos domaines d'expertises
description: Une approche transversale de vos dossiers.
date: '2019-02-28'
aliases:
    - /competences/
---

Le Cabinet dispose d’une solide expérience dans les domaines liés à la **Propriété Intellectuelle** et notamment les **Nouvelles Technologies de l’Information** (Internet, mobile, etc).

Cela nous permet de vous accompagner au travers d’une **approche polyvalente, active, réactive et confiante** tout au long de votre dossier afin d’adapter, avec pertinence et stratégie, vos besoins à vos réalités économiques et aux lois en vigueur.

