+++
aliases = ["/avocat-propriete-intellectuelle"]
bgcolor = "#9684A1"
description = "Droit d'auteur, logiciels, applications, marques, brevets, dessins, modèles : nous protégeons et valorisons vos créations"
h3 = "Propriété intellectuelle"
icon = "/images/icons/icon_copyright.png"
meta_description = "Droit d'auteur, logiciels, applications, marques, brevets, dessins, modèles : nous protégeons et valorisons vos créations."
slug = ""
title = "Propriété intellectuelle et industrielle"
weight = 1
id_expertise = 2
[menu.footer2]
name = "Propriété intellectuelle"
weight = 1
[menu.main]
name = "Propriété intellectuelle"
parent = "Expertises"
weight = 1

+++
### Propriété industrielle
La propriété industrielle a pour objet la protection et la valorisation des inventions, des innovations et des créations.
Le cabinet intervient pour le compte de sociétés ou de particuliers dans les domaines suivants :
- Recherche d’antériorité - Dépôt et stratégie de protection de marques
- Dépôt et stratégie de protection de dessins et modèles
- Noms de domaine : dépôt, récupération, arbitrage
- Assistance et stratégie dans la protection des brevets 
- Cession de marques, brevets ou dessins et modèles
- Licence de marques, brevets ou dessins et modèles

Le cabinet traite des problématiques notamment dans la protection des marques, dessins, et modèles par le biais de contrats / licences ainsi que dans le cadre de contentieux juridiques (saisies contrefaçon, assignation ou défense).


#### Quelques applications
-   Négociation et rédaction de contrats (accords transactionnels, de confidentialité et de coexistence, contrats de cession ou de licence, etc.)
-   Audit et stratégie de portefeuilles de droits de propriété intellectuelle
-   Procédures d’opposition devant l’INPI et l’OHMI.
-   Actions en contrefaçon, en déchéance, en nullité, en dégénérescence, en revendication de propriété, référé interdiction provisoire, etc.

### Droits d’auteur 
Nous intervenons tant pour le compte de sociétés, de particuliers, d’artistes dans les domaines suivants :

-   Oeuvres littéraires,
-   Oeuvres musicales,
-   Oeuvres graphiques et plastiques…
-   Photographies
-   Bases de données
-   Logiciels/applications/site internet
-   IA

Le cabinet assiste ses clients dans la protection, la veille, et la défense de leurs droits d’auteurs. Le cabinet intervient dans le cadre de l’élaboration de contrats ainsi que la négociation des droits (cession, licence, accords de distribution, etc).

#### Quelques applications

-   Action en contrefaçon, en demande ou en défense
-   Action en concurrence déloyale, en demande ou en défense
-   Action en protection de votre droit à l’image et à la vie privée
-   Négociation de contrats de cession de droits d’auteur
-   Négociation de contrats de licence de droits d’auteur
-   Rédaction, négociation de contrats d'édition
-   Rédaction de contrats et d’accord par exemple de mise à disposition, distribution etc
-   Gestion des créations salariées
-   Protection des bases de données