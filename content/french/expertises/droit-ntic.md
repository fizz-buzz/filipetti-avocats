+++
aliases = ["/droit-des-ntic"]
bgcolor = "#f4a261"
description = "Nouvelles technologies et digital : une prise en charge à 100 %"
h3 = "Digital"
icon = "/images/icons/icon_digital.png"
meta_description = "Nous accompagnons de nombreux clients sur l’ensemble des problématiques liées aux Nouvelles Technologies de l’Information, la publicité et la communication, la Propriété Intellectuelle et la concurrence déloyale, le droit de la Consommation, le droit de la distribution."
slug = "avocat-digital-nouvelles-technologies"
title = "Droit des nouvelles technologies"
weight = 2
id_expertise = 1
[menu.footer2]
name = "Nouvelles technologies"
weight = 2
[menu.main]
name = "Nouvelles technologies"
parent = "Expertises"
weight = 2

+++
### Les nouvelles technologies et le digital – la force d’un accompagnement complet
Isabelle Filipetti est titulaire du DESS en « Droit de l’informatique et du multimédia » à l’Université Paris 2 Pantheon Assas. Sa formation et son expérience lui permettent de conseiller et d’accompagner de nombreux clients et acteurs du e-commerce sur l’ensemble des problématiques liées aux Nouvelles Technologies de l’Information, la publicité et la communication, la Propriété Intellectuelle et la concurrence déloyale, le droit de la Consommation, le droit de la distribution.

### Domaines de compétences à votre disposition pour la pérennité des acteurs du e-commerce, start-up
-   Droit de la propriété intellectuelle appliquée à Internet, les sites et les applications mobiles
-   Droit de l’informatique et de l’intelligence artificielle
-   Accompagnements et problématiques tant judiciaires que juridiques dans le cadre du e-commerce : mentions obligatoires, mentions essentielles, publicité, CGV, CGUV, charte de confidentialité politique en matière de cookies, noms de domaine, marques
-   Règles sur la publicité suivant les secteurs/e-marketing
-   Protection des données personnelles sur Internet / CNIL / RGPD
-   Concurrence déloyale : imitation de produits, pratiques commerciales déloyales, parasitisme
-   Usurpation de dénomination sociale, de nom commercial, de nom de domaine
-   Diffamation
-   Vie privée / droit à l’image
-   Litiges commerciaux et de consommation

### Exemples de nos accompagnements
-   Rédaction de contrats de prestation de services dans le domaine du digital : référencement, affiliation, influenceur, distribution, freelance, etc
-   Accords de confidentialité, NDA
-   Aide à la création d’un site – d’une application / validation des sites et application par le biais d’un audit en conformité avec les règles du droit à la consommation et les RGPD
-   Cession des droits de propriété intellectuelle sur un site Internet, logiciel, applications
-   Licence d’utilisation des droits d’exploitation d’éléments protégés par la propriété intellectuelle
-   Contrats informatiques, de maintenance
-   Rédaction des conditions générales de vente et d’utilisation d’un site Internet, mentions légales
-   Rédaction de contrats en droit de la propriété intellectuelle, édition, droit de l’informatique, NTIC

### Audits
-   Audit des droits de propriété industrielle
-   Audit des droits de propriété littéraire et artistique
-   Audit RGPD et mise en conformité
-   Audit de sites et applications BtoB, BtoC, CtoC, etc