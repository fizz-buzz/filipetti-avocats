+++
aliases = []
bgcolor = "#91A6FF"
description = "Une expertise au service de l'enseignement"
h3 = "Formations"
icon = "/images/icons/icon_formation.png"
meta_description = "Le Cabinet intervient dans le cadre de formations privées en droit du travail, droit social, propriété intellectuelle, droit à l’image et vie privée."
slug = "avocat-formations-juridiques"
title = "Formations & enseignement"
weight = 5
id_expertise = 6

[menu.footer2]
name = "Formations"
weight = 5
[menu.main]
name = "Formations"
parent = "Expertises"
weight = 5
+++
### Formations 
Le Cabinet intervient dans le cadre de formations privées autour des thèmes suivants :

-   Droit du travail
-   Droit social
-   Propriété intellectuelle
-   Droit à l’image et vie privée
-   Conseil sur Internet

### Enseignement 
Isabelle FILIPETTI intervient auprès de l’Université de droit de Nice Sophia-Antipolis et dans des écoles spécialisées dans les NTIC (Nouvelles Technologies de l’Information et de la Communication) pour transmettre son expertise en :

-   Droit à l’image
-   Droit d’auteur
-   Droit des marques
-   Droit du numérique
-   Droit du travail