+++
aliases = ["/avocat-droit-des-affaires"]
bgcolor = "#e9c46a"
description = "Une approche transversale de la vie juridique de votre société : un suivi dès sa création"
h3 = "Droit des affaires"
icon = "/images/icons/icon_droit-affaires.png"
meta_description = "Le cabinet est particulièrement impliqué dans l’élaboration, la négociation et la rédaction de contrats pour toutes opérations économiques et commerciales."
slug = "avocat-droit-societes"
title = "Avocat en droit des affaires et droit commercial"
weight = 4
id_expertise = 4

[menu.footer2]
name = "Droit des affaires"
weight = 4
[menu.main]
name = "Droit des affaires"
parent = "Expertises"
weight = 4

+++
### Droit des affaires

Nous assistons notamment nos clients sur les problématiques suivantes en droit des sociétés et droit commercial :

-   Représentation devant tous les Tribunaux de Commerce en France métropolitaine
-   Recouvrement par voie amiable ou judiciaire de vos créances
-   Transfert ou cession de fonds de commerce
-   Baux commerciaux

#### Mais aussi dans les relations commerciales par le biais de la rédaction et de la négociation des contrats 
Le cabinet est particulièrement impliqué dans l’élaboration, la négociation et la rédaction de contrats pour toutes opérations économiques et commerciales (contrat de prestation de services, accords de distribution, licence, cession, tout contrat avec des problématiques en droit de la propriété intellectuelle, droit de l’informatique notamment, etc) notamment :

-   Contrat de confidentialité et NDA
-   Contrat de prestation de services
-   Contrat de développement informatique
-   Contrat de partenariat
-   Contrat de distribution
-   Contrat d’agence commerciale
-   Contrat d’apport d’affaires
-   CGV
-   CGU

### Vie des sociétés
Le cabinet intervient dans le cadre de la constitution, la transformation et la dissolution de votre société (civile ou commerciale)

#### De l’immatriculation
- Statuts SAS
- Pacte fondateurs

#### A son développement et évolution
-   Augmentation de capital
-   Pacte d’associés
-   Déclarations des dirigeants
-   Garanties de passif
-   Réduction de capital
-   Cession d’actions (ou promesses)
-   Assemblées générales
-   Transformation de la société
-   Liquidation amiable

Notre cabinet assiste le dirigeant dans le cadre d’une procédure de redressement ou de liquidation judiciaire.

Isabelle FILIPETTI est membre de l’Association des Avocats Praticiens en Droit des Affaires du 06.