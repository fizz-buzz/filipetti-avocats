---
title: Avocat Sophia Antipolis
description: Sophia Antipolis - les prestations de notre cabinet d'avocat
ville: Sophia Antipolis
codepostal: "06450"
population: 9102
tgi: Grasse
ti: Antibes
tc: Antibes
cph: Cannes
courappel: Aix-en-Provence
---