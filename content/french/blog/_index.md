---
title: Actualités
description: Actualités juridiques et vie du cabinet.
bg_image: images/featue-bg.jpg
aliases:
- "/publications/"
- "/category/conferences/"
menu:
  footer1:
    weight: 4
  main:
    url: blog
    weight: 4

---
