---
title: Sensibilisation juridique autour des réseaux sociaux
date: 2013-03-08T23:00:00+00:00
description: Nos droits sur nos créations, comment nous pouvons les utiliser et dans
  quelles limites les contenus sur les réseaux sociaux peuvent être diffusés
type: post
tags:
- Réseaux sociaux
categories:
- Conférences
aliases:
- "/sensibilisation-juridique-reseaux-sociaux/"

---
Les réseaux sociaux sont utilisés par tous tant de manière professionnelle que personnelle de sorte que **naissent de plus en plus de problématiques quant à l’appropriation des contenus ainsi que des créations** et par conséquent, leur diffusion. 

C’est pourquoi, il nous a semblé essentiel au regard du coeur de nos métiers de rappeler à chacun quels sont nos droits sur nos créations, comment nous pouvons les utiliser et dans quelles limites les contenus sur les réseaux sociaux peuvent être diffusés sans violer le droit des tiers. 

Il nous a paru également important de donner un éclairage juridique du droit à l’image face aux réseaux sociaux, des règles contractuelles d’utilisation auxquelles nous sommes astreints et des répercussions engendrées en droit du travail.

<iframe style="border:0; margin-bottom: 5px;" src="http://fr.slideshare.net/slideshow/embed_code/17769010" width="100%" height="450" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<a title="Sensibilisation juridique autour des réseaux sociaux" href="http://fr.slideshare.net/filipetti-avocat/sensibilisation-juridique-autour-des-rseaux-sociaux" target="_blank" rel="noopener">Sensibilisation juridique autour des réseaux sociaux</a> </strong> from <strong><a href="http://fr.slideshare.net/filipetti-avocat" target="_blank" rel="noopener">filipetti-avocat</a>