+++
aliases = []
categories = []
date = 2021-11-11T23:00:00Z
description = "AXE AVOCATS"
tags = []
title = "De grands changements pour 2022 !"
type = "post"

+++
En 2022, le Cabinet va fusionner avec celui de NH Avocats, partenaire depuis des années. Pour marquer ce rapprochement, nous avons décidé de nous appeler AXE pour rappeler à tous nos Clients qu'ensemble nous avons une trajectoire stratégique à définir - celle de votre réussite, celle qui vous convient, celle qui vous rassure et celle qui vous protégera. Nous vous en dirons plus au moment voulu.... A très vite