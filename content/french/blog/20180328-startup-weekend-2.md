---
title: Seconde participation au Startup Weekend Sophia Antipolis
date: 2018-03-28
description: Participation d’Isabelle FILIPETTI en tant que coach et sponsor en qualité
  d’avocat en droit de la propriété intellectuelle pour la 2e année consécutive au
  STARTUP WEEKEND
type: post
tags:
- Startup
categories:
- Conférences
aliases:
- "/2nde-participation-startup-weekend-isabelle-filipetti-avocat/"

---
Participation d’Isabelle FILIPETTI en tant que coach et sponsor en qualité d’avocat en droit de la propriété intellectuelle pour la 2e année consécutive au STARTUP WEEKEND (mars 2013) organisé par le Skema Business School – Sophia Antipolis 
www.nice.startupweekend.org