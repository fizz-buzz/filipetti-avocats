---
title: Conférence d’Isabelle Filipetti dans le cadre du STARTUP WEEKEND
date: 2018-01-29
description: Participation d’Isabelle FILIPETTI en tant que coach et sponsor en qualité
  d’avocat en droit de la propriété intellectuelle pour la 2e année consécutive au
  STARTUP WEEKEND
type: post
tags:
- Startups
categories:
- Conférences
aliases:
- "/conference-dans-le-cadre-du-startup-week-end/"

---
Les questions principales en droit de la propriété intellectuelle pour les futurs entrepreneurs

http://nice.startupweekend.org