---
title: La propriété intellectuelle et l’avocat
date: 2017-03-09
description: Quelle relation y a-t-il entre la propriété intellectuelle et le métier
  d’avocat ?
type: post
tags:
- internet
categories:
- Publications
aliases:
- "/propriete-intellectuelle-et-avocat/"

---
### Quelle relation y a-t-il entre la propriété intellectuelle et le métier d’avocat ?
Tout d’abord, celle-ci peut se résumer par la notion de conseil que nous aborderons dans la première partie ci-après.

En effet, que vous soyez artiste ou une entreprise, vos besoins nécessitent des conseils tant au niveau des contrats pour les artistes – notamment contrats d’édition ou de cession – que pour les entreprises concernant l’orientation et la protection de votre marque.

A ce titre, la protection de la marque dès sa création est d’une importance malheureusement trop peu mesurée. En effet, outre le fait que celle-ci soit le pilier de la communication et de la publicité, une marque constitue le patrimoine incorporel de la société ainsi que le nom de domaine associé. C’est pourquoi, il est essentiel de chercher un signe libre de toute antériorité afin de préserver ce patrimoine naissant et permettre sa future valorisation.

La recherche d’antériorité à l’identique et en similaire est essentielle à ce stade afin de déterminer le risque d’investir sur le signe escompté. La protection de la marque passe aussi par son dépôt auprès de l’INPI et l’enregistrement du signe à titre de nom de domaine. Le dépôt devra viser le plus précisément possible les classes et les services avec un intitulé approprié qui définiront l’usage de la marque afin de réellement optimiser et valoriser celle-ci dans le cadre de l’activité exploitée.

Concernant les artistes et plus particulièrement le droit d’auteur qui leur est applicable, des protections en amont peuvent être envisagées telles que le dépôt de dessins et modèles suivant le type de création, des enveloppes « soleau » ou constat d’huissier afin de prendre date en cas de contrefaçon.

Il en est de même concernant la création de site Internet laquelle bénéficie de la protection du droit d’auteur.

Le rôle de l’avocat est de vous conseiller et de vous accompagner dans l’ensemble de ces démarches afin de protéger le plus précisément possible en amont et de valoriser votre patrimoine immatériel. Malheureusement, cette phase préalable est trop souvent ignorée.