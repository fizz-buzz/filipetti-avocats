---
title: Droit à l’image et droit internet (conférence)
date: 2011-03-31
description: Conférence organisée par Fenêtre sur Com’, en partenariat avec l’hôtel
  5 étoiles Le NEGRESCO Made in French Riviera.
type: post
tags:
- Internet
categories:
- Conférences
aliases:
- "/sensibilisation-juridique-reseaux-sociaux/"

---
**Droit à l’image et droit internet (31 mars 2011)**
Conférence organisée par Fenêtre sur Com’, en partenariat avec l’hôtel 5 étoiles Le NEGRESCO Made in French Riviera.

**Intervenants :** 

Maître Isabelle FILIPETTI, Avocat au Barreau de Grasse, Diplômée du DESS Droit du multimédia et de l’informatique-Paris II ASSAAS (2001) Domaines d’intervention principaux : propriété intellectuelle et les questions liées à l’internet

Patrick GAUTHEY, photographe

Michel BOUNOUS, Editions Baie des Anges

Martine CLEMENTE, Déléguée régionale, INPI

Giovanni GULINO, Directeur Marketing Hôtel Le Negresco

Liens vers la page de l’événement par Fenêtre sur Com’: http://www.fenetresurcom.org/evenement/compte-rendu-de-la-conference-du-31-mars-sur-le-droit-limage-et-droit-internet

Lien vers les extraits audio par presse Alpes Maritimes :  http://www.pressealpesmaritimes.com/?p=10087