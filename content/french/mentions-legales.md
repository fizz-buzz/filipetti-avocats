+++
aliases = ["mentions-legales"]
description = "Mentions légales du site."
title = "Mentions légales"
[menu.copyright]
name = "Mentions légales"
weight = 4

+++

**Conformément aux dispositions de l’article 6 III-1 de la Loi No 2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique mise à jour avec les RGPD** , nous vous informons que :

Ce site est la propriété de :

Isabelle Filipetti IF AVOCATS – cabinet d’Avocats

257 Avenue Saint Exupéry
1er étage
06700 Saint-Laurent du Var

Le directeur de la publication est Isabelle Filipetti

Le stockage direct et permanent est fourni par :
Gandi
15 place de la Nation
75011 FRANCE


Réalisation [Fizzbuzz](https://fizzbuzz.fr)