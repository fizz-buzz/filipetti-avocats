+++
description = "Consultation avocat en ligne"
title = "Consultation avocat en ligne"

[menu.footer1]
name = "Consultations en ligne"
weight = 3

+++
## Démarches en ligne - Maître Isabelle Filipetti

### Prendre RDV en ligne

Maître Filipetti vous reçoit à son cabinet pour un 1er rendez-vous. Ce premier rdv de 60 min, sans engagement, permet réciproquement de faire connaissance, de connaître vos droits et la manière de traiter la problématique à laquelle vous êtes confronté.


[Prendre rdv en ligne](https://consultation.avocat.fr/avocat-44892-7e6d.html)

### Demander un rappel

Maître Filipetti vous rappelle pour répondre à vos questions juridiques. Cette consultation téléphonique de 20 min vous permet d'obtenir des éléments de réponse concrets et rapides à la question que vous vous posez.

[Demander un rappel](https://consultation.avocat.fr/avocat-44892-7e6d.html)

### Consulter par écrit

Maître Filipetti répond à vos questions juridiques par écrit. Si vous souhaitez accompagner votre question d'une pièce jointe, la consultation juridique est adaptée, sinon la question simple (moins de 1.000 caractères) répondra parfaitement à votre question de droit.

[Consulter par écrit](https://consultation.avocat.fr/avocat-44892-7e6d.html)


