+++
bg_image = ""
description = "Nos domaines d’intervention principaux sont le droit des nouvelles technologies, le droit de la propriété intellectuelle et le droit du travail"
layout = "services"
title = "Services"

+++
## Isabelle Filipetti

Isabelle FILIPETTI est avocat au Barreau de Grasse (06). Titulaire d’une maîtrise en droit privé et d’un DESS de droit de Propriété Intellectuelle et du multimédia de l’université Panthéon-Assas (Paris II), elle a prêté serment à Paris en 2004 et a été une collaboratrice engagée au sein de cabinets parisiens et niçois renommés.

En 2010, Isabelle FILIPETTI crée son cabinet en s'entourant d'une équipe dynamique partageant son niveau d'exigence. Avec comme motivation première la réussite de tous les projets confiés à son cabinet, Isabelle FILIPETTI met à la disposition de ses clients toute sa compétence, sa rigueur, son écoute et sa polyvalence. Le cabinet intervient pour conseiller, prévenir ou gérer un conflit, tant pour les **particuliers** que les **entreprises**, **sociétés**, **PME**, et **salariés**.

Isabelle FILIPETTI est membre de l’Association des Avocats Praticiens en Droit des Affaires du 06 et a développé de nombreux partenariats avec des avocats en France dans les domaines d'interventions listés.

## Une expertise au service de l'enseignement

Isabelle FILIPETTI intervient auprès de l’Université de droit de Nice Sophia-Antipolis et dans des écoles spécialisées dans les NTIC (Nouvelles Technologies de l'Information et de la Communication) pour transmettre son expertise en droit à l’image, droit d’auteur, droit des marques, droit du numérique ainsi qu'en droit du travail.

Isabelle FILIPETTI intervient également dans le cadre de **formations privées autour du droit du travail, droit social, propriété intellectuelle et conseil sur Internet**.